export default {
  cognito: {
    REGION: "us-west-2",
    USER_POOL_ID: "us-west-2_7EzkDES1o",
    APP_CLIENT_ID: "2mpudrttvi6pd9ofpjmt92iilq",
    IDENTITY_POOL_ID: "us-west-2:1b33a261-9aa1-47ed-a763-b6a1a094f3cc",
    oauth: {
      domain: "devesan.auth.us-west-2.amazoncognito.com",
      scope: ["email", "openid", "profile"],
      redirectSignIn: "http://localhost:3000/login/",
      redirectSignOut: "http://localhost:3000/login/",
      responseType: "token"
    }
  },

  social: {
    FB: "3169149186434332"
  }
};

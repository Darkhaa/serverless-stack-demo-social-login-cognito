import React, { Component } from "react";
import { Auth, Hub } from "aws-amplify";
import LoaderButton from "./LoaderButton";
import queryString from "query-string";

export default class FacebookButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    const { location } = this.props;
    const parsedHash = queryString.parse(location.hash);
    if (parsedHash.error || parsedHash.error === "invalid_request") {
      alert(parsedHash.error_description);
    } else {
      Hub.listen("auth", data => {
        const { payload } = data;
        console.log(payload);
        if (payload.event === "signIn") {
          this.setState({ isLoading: false });
          this.props.onLogin(payload.data);
        }
      });
    }
    this.setState({ isLoading: false });

    this.checkUser();
  }

  async checkUser() {
    try {
      const user = await Auth.currentAuthenticatedUser();
      console.log("user: ", user);
    } catch (err) {
      console.log("err: ", err);
    }
  }

  render() {
    return (
      <LoaderButton
        block
        bsSize="large"
        bsStyle="primary"
        className="FacebookButton"
        text="Login with Google"
        onClick={() => Auth.federatedSignIn({ provider: "Google" })}
        disabled={this.state.isLoading}
      />
    );
  }
}

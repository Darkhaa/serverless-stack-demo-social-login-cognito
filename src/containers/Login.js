import React, { Component } from "react";
import { Auth } from "aws-amplify";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import FacebookButton from "../components/FacebookButton";
import GoogleButton from "../components/GoogleButton";
import axios from "axios";
import "./Login.css";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleSocialLogin = () => {
    this.props.userHasAuthenticated(true);
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

   handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    axios
      .post("https://ran1hsubw2.execute-api.us-west-2.amazonaws.com/dev", {
        email: this.state.email
      })
      .then(response => {
        let responseData = JSON.parse(response.data.errorMessage);
        if (responseData.is_registed) {
          if (responseData.is_registed_type === 0) {
            try {
              Auth.signIn(this.state.email, this.state.password);
              this.props.userHasAuthenticated(true);
            } catch (e) {
              alert(e.message);
              this.setState({ isLoading: false });
            }
          } else {
            alert(`${responseData.messega}`);
          }
        } else {
          alert("User not found");
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        console.log(error);
        this.setState({ isLoading: false });
      });
  };

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FacebookButton
            onLogin={this.handleSocialLogin}
            location={this.props.location}
          />
          <GoogleButton
            onLogin={this.handleSocialLogin}
            location={this.props.location}
          />
          <hr />
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Logging in…"
          />
        </form>
      </div>
    );
  }
}
